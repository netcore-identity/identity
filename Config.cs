﻿
namespace Identity
{
  using IdentityServer4;
  using IdentityServer4.Models;
  using IdentityServer4.Test;
  using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Security.Claims;

  public static class Config
  {
    public static IEnumerable<ApiResource> GetApiResources()
    {
      return new List<ApiResource>
                    {
                        new ApiResource
                        {
                            Name = "api",
                            Description = "Identity Server API",
                            DisplayName = "Identity Server API",
                            Enabled = true,
                            Scopes =
                            {
                                new Scope()
                                {
                                    Name = "identity",
                                    DisplayName = "Access to Identity Server API",
                                    ShowInDiscoveryDocument = true,
                                    UserClaims = {
                                        IdentityServerConstants.StandardScopes.Email,
                                        IdentityServerConstants.StandardScopes.Profile,
                                        Constants.Claims.Permission,
                                        "role"
                                    },
                                 },
                            }
                        }
                    };
    }

    public static IEnumerable<Client> GetClients()
    {
      return new List<Client>
            {
                // resource owner password grant client
                new Client
                {
                    ClientName = "Identity Server Website client",
                    ClientId = "identity",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AccessTokenLifetime = 3600 * 24,

                    AllowAccessTokensViaBrowser = true,
                  
                    RedirectUris = { "http://localhost:5000/silent-refresh.html" },

                    RequireConsent = false,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    AllowOfflineAccess = true,

                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "roles",
                        "identity"
                    }
                },
            };
    }

    public static IEnumerable<TestUser> GetUsers()
    {
      yield return new TestUser
      {
        SubjectId = "1",
        Username = "kbeaugrand@gmail.com",
        Password = "password",

        Claims = GetAdminClaims().ToList()
      };
    }

    public static IEnumerable<Claim> GetAdminClaims()
    {
      yield return new Claim(ClaimTypes.Role, Constants.Roles.IdentityFullAccess);
      yield return new Claim(ClaimTypes.Email, "kbeaugrand@gmail.com");
      yield return new Claim(ClaimTypes.DateOfBirth, DateTimeOffset.Now.ToString("o"));
      yield return new Claim(ClaimTypes.Gender, "0");
      yield return new Claim(ClaimTypes.Surname, "administrator");
      yield return new Claim(ClaimTypes.GivenName, "General");
    }

    public static IEnumerable<IdentityRole> GetRoles()
    {
      yield return new IdentityRole(Constants.Roles.IdentityReadOnly)
      {
        Claims = {
          new IdentityRoleClaim<string>{ ClaimType = Constants.Claims.Permission, ClaimValue = Constants.Permissions.UsersReadOnly },
          new IdentityRoleClaim<string>{ ClaimType = Constants.Claims.Permission, ClaimValue = Constants.Permissions.RolesReadOnly }
        }
      };

      yield return new IdentityRole(Constants.Roles.IdentityFullAccess)
      {
        Claims = {
          new IdentityRoleClaim<string>{ ClaimType = Constants.Claims.Permission, ClaimValue = Constants.Permissions.UsersReadOnly },
          new IdentityRoleClaim<string>{ ClaimType = Constants.Claims.Permission, ClaimValue = Constants.Permissions.RolesReadOnly },
          new IdentityRoleClaim<string>{ ClaimType = Constants.Claims.Permission, ClaimValue = Constants.Permissions.UsersFullaccess },
          new IdentityRoleClaim<string>{ ClaimType = Constants.Claims.Permission, ClaimValue = Constants.Permissions.RolesFullaccess }
        }
      };
    }

    public static IEnumerable<IdentityResource> GetIdentityResources()
    {
      yield return new IdentityResources.OpenId();
      yield return new IdentityResources.Profile();
      yield return new IdentityResources.Email();
      yield return new IdentityResource("roles", "User roles", new[] { "role" })
      {
        ShowInDiscoveryDocument = false,
      };
    }
  }
}
