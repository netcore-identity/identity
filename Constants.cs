
namespace Identity
{
  public static class Constants
  {
    public static class Claims {
      public const string  Permission = "http://example.org/claims/permission";
    }

    public static class Roles
    {
      public const string IdentityReadOnly = "identity.readonly";
      
      public const string IdentityFullAccess = "identity.fullaccess";
    }

    public static class Permissions
    {
      public const string UsersReadOnly = "users.readonly";

      public const string UsersFullaccess = "users.fullaccess";

      public const string RolesReadOnly = "users.readonly";
      
      public const string RolesFullaccess = "users.fullaccess";
    }
  }
}