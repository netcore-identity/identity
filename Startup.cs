﻿
namespace Identity
{
  using Identity.Data;
  using Identity.Models;
  using IdentityServer4.EntityFramework.DbContexts;
  using IdentityServer4.EntityFramework.Mappers;
  using Microsoft.AspNetCore.Builder;
  using Microsoft.AspNetCore.Hosting;
  using Microsoft.AspNetCore.Http;
  using Microsoft.AspNetCore.Identity;
  using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
  using Microsoft.EntityFrameworkCore;
  using Microsoft.Extensions.Configuration;
  using Microsoft.Extensions.DependencyInjection;
  using Microsoft.Extensions.FileProviders;
  using Microsoft.Extensions.Logging;
  using System;
  using System.IO;
  using System.Linq;
  using System.Reflection;
  using System.Security.Claims;
  using Microsoft.IdentityModel.Tokens;
  using System.Security.Cryptography;
  using Microsoft.AspNetCore.DataProtection;
  using System.Threading.Tasks;
  using Identity.Mailing;

  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();

      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

      // Add framework services.
      services.AddMvc();

      services.AddDataProtection();

      services.AddDbContext<ApplicationDbContext>(c =>
      {
        c.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), o => o.MigrationsAssembly(migrationsAssembly));
      });

      services.AddIdentity<ApplicationUser, IdentityRole>(o =>
      {
        o.Password.RequireDigit = false;
        o.Password.RequireNonAlphanumeric = false;
        o.Password.RequireUppercase = false;
        o.Password.RequiredLength = 1;
        o.Password.RequireLowercase = false;

        o.User.RequireUniqueEmail = true;
        o.Lockout.MaxFailedAccessAttempts = 5;
        o.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
      })
          .AddEntityFrameworkStores<ApplicationDbContext>()
          .AddDefaultTokenProviders();

      // configure identity server with in-memory users, but EF stores for clients and resources
      services.AddIdentityServer()
          .AddDeveloperSigningCredential()
          .AddAspNetIdentity<ApplicationUser>()
          .AddConfigurationStore(builder =>
              builder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), options =>
                  options.MigrationsAssembly(migrationsAssembly)))
          .AddOperationalStore(builder =>
              builder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), options =>
                  options.MigrationsAssembly(migrationsAssembly)));

      services.AddAuthorization(options =>
      {
        options.AddPolicy("Users Readonly", policy =>
        {
          policy.RequireClaim(Constants.Claims.Permission, Constants.Permissions.UsersReadOnly);
        });

        options.AddPolicy("Users FullAccess", policy =>
        {
          policy.RequireClaim(Constants.Claims.Permission, Constants.Permissions.UsersFullaccess);
        });

        options.AddPolicy("Roles Readonly", policy =>
        {
          policy.RequireClaim(Constants.Claims.Permission, Constants.Permissions.RolesReadOnly);
        });

        options.AddPolicy("Roles FullAccess", policy =>
        {
          policy.RequireClaim(Constants.Claims.Permission, Constants.Permissions.RolesFullaccess);
        });
      });

      services.AddSingleton<IConfiguration>(x => this.Configuration);

      services.AddTransient<MailBuilder, MailBuilder>();
      services.Configure<MailConfiguration>(this.Configuration.GetSection("Mail"));
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      loggerFactory.AddConsole(LogLevel.Debug);

      if (env.IsDevelopment())
      {
        EnsureDatabaseDeleted(app);
        app.UseDeveloperExceptionPage();
      }

      // this will do the initial DB population
      InitializeDatabase(app);

      var angularRoutes = new[] {
                "/admin",
                "/login",
                "/register",
                "/unauthorized"
            };

      app.Use(async (context, next) =>
      {
        if (context.Request.Path.HasValue &&
                  angularRoutes.Any((ar) => context.Request.Path.Value.StartsWith(ar, StringComparison.OrdinalIgnoreCase)))
        {
          context.Request.Path = new PathString("/");
        }

        await next();
      });

      app.UseDefaultFiles();
      app.UseStaticFiles();

      app.UseIdentity();

      // Adds IdentityServer
      app.UseIdentityServer();

      app.UseJwtBearerAuthentication(new JwtBearerOptions
      {
        Authority = "http://localhost:5000/", // base address of your OIDC server.
        Audience = "api", // base address of your API.
        RequireHttpsMetadata = false
      });

      app.UseMvc();
    }

    private void EnsureDatabaseDeleted(IApplicationBuilder app)
    {
      using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
      {
        serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>().Database.EnsureDeleted();
        serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.EnsureDeleted();
        serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>().Database.EnsureDeleted();
      }
    }

    private async void InitializeDatabase(IApplicationBuilder app)
    {
      using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
      {
        var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
        var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

        serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

        var applicationDbContext = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        var configurationDbContext = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();

        applicationDbContext.Database.Migrate();
        configurationDbContext.Database.Migrate();

        if (!configurationDbContext.Clients.Any())
        {
          foreach (var client in Config.GetClients())
          {
            configurationDbContext.Clients.Add(client.ToEntity());
          }
          configurationDbContext.SaveChanges();
        }

        if (!configurationDbContext.IdentityResources.Any())
        {
          foreach (var resource in Config.GetIdentityResources())
          {
            configurationDbContext.IdentityResources.Add(resource.ToEntity());
          }
          configurationDbContext.SaveChanges();
        }

        if (!configurationDbContext.ApiResources.Any())
        {
          foreach (var resource in Config.GetApiResources())
          {
            configurationDbContext.ApiResources.Add(resource.ToEntity());
          }
          configurationDbContext.SaveChanges();
        }

        if (!applicationDbContext.Roles.Any())
        {
          foreach (var role in Config.GetRoles())
          {
            await roleManager.CreateAsync(role);
          }
        }

        if (applicationDbContext.Users.Any())
        {
          return;
        }

        foreach (var user in Config.GetUsers())
        {
          var createdUser = new ApplicationUser()
          {
            UserName = user.Username,
            Email = user.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email)?.Value
          };

          var result = await userManager.CreateAsync(createdUser, user.Password);

          if (!result.Succeeded)
          {
            throw new InvalidOperationException(result.Errors.ToString());
          }

          if (user.Claims == null || !user.Claims.Any())
          {
            continue;
          }

          foreach (var claim in user.Claims)
          {
            createdUser.Claims.Add(new IdentityUserClaim<string>
            {
              ClaimType = claim.Type,
              ClaimValue = claim.Value
            });
          }

          foreach (var role in user.Claims?.Where(c => c.Type == ClaimTypes.Role)
                       .Select(c => c.Value)
                       .Distinct()
                       .ToArray())
          {
            result = await userManager.AddToRoleAsync(createdUser, role);

            if (!result.Succeeded)
            {
              throw new InvalidOperationException(result.Errors.ToString());
            }
          }

          var mailBuilder = serviceScope.ServiceProvider.GetRequiredService<MailBuilder>();

          await mailBuilder.StartNew()
                      .To(createdUser.Email)
                      .Subject($"{this.Configuration["WebsiteName"]} is up!")
                      .AppendLine("## Site up and running")
                      .AppendLine("Hi guy, you website is now up and running.")
                      .AppendLine("")
                      .AppendLine("You can now access with this logins:")
                      .AppendLine($"- {createdUser.Email}")
                      .AppendLine($"- {user.Password}")
                      .AppendLine("**Don't forget to change your password as soon as possible!**")
                      .AppendLine("")
                      .AppendLine("Hope you will have fun with this new website.")
                      .Send();
        }
      }
    }
  }
}
