
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace Identity.Controllers
{
  using IdentityServer4.EntityFramework.DbContexts;
  using IdentityServer4.EntityFramework.Entities;
  using Microsoft.AspNetCore.Authorization;
  using Microsoft.AspNetCore.Mvc;
  using System.Collections.Generic;
  using System.Linq;
  using Identity.Data;
  using Identity.Models;
  using Microsoft.EntityFrameworkCore;
  using System;
  using System.Security.Claims;
  using Microsoft.AspNetCore.Identity;
  using Models.Extensions;
  using System.Threading.Tasks;
  using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
  using Identity.Extensions;
  using Identity.Mailing;
  using Microsoft.Extensions.Configuration;

  [Route("api/[controller]")]
  public class UsersController : Controller
  {
    ApplicationDbContext context;
    UserManager<ApplicationUser> userManager;
    MailBuilder mailBuilder;
    IConfiguration configuration;

    public UsersController(
      ApplicationDbContext context, 
      UserManager<ApplicationUser> userManager,
      MailBuilder mailBuilder,
      IConfiguration configuration
    )
    {
      this.context = context;
      this.userManager = userManager;
      this.mailBuilder = mailBuilder;
      this.configuration = configuration;
    }

    // GET: api/clients
    [HttpGet]
    [Authorize("Users Readonly")]
    public IEnumerable<ApplicationUser> Get()
    {
      var result = context
          .Users
          .Include(x => x.Claims)
          .ToArray();

      return result;
    }

    // GET api/clients/5
    [HttpGet("{id}")]
    [Authorize("Users Readonly")]
    public async Task<ActionResult> Get(string id)
    {
      var user = await context.Users
          .Include(x => x.Claims)
          .Include(x => x.Roles)
          .Include(x => x.Logins)
          .SingleOrDefaultAsync(x => x.Id == id);

      if (user == null)
      {
        return this.NotFound();
      }

      var roles = await context.Roles
                            .Where(x => user.Roles.Any(r => r.RoleId == x.Id))
                            .ToArrayAsync();

      return this.Ok(new UserDetail
      {
        Email = user.Email,
        Roles = roles.Select(x => x.Name),
        Claims = user.Claims
            .Where(x => x.ClaimType != ClaimTypes.Role)
            .Select(x => new UserDetailClaim { ClaimType = x.ClaimType, ClaimValue = x.ClaimValue }),

      });
    }

    // POST api/clients
    [HttpPost]
    [Authorize("Users Readonly")]
    public async Task<ActionResult> Post([FromBody]UserDetail value)
    {
      var user = new ApplicationUser
      {
        UserName = value.Email,
        Email = value.Email
      };
      
      this.UpdateClaims(user, value);

      var result = await this.userManager.CreateAsync(user);

      if (!result.Succeeded)
      {
        return BadRequest(result.Errors);
      }

      await this.userManager.UpdateRolesAsync(user, value.Roles);

      context.SaveChanges();

      var hostUrl = $"{this.Request.Scheme}://{this.Request.Host}";

      await this.mailBuilder
                .StartNew()
                .To(user.Email)
                .Subject($"You have a new account to {configuration["WebsiteName"]}")
                .AppendLine($"# You have been invited to {configuration["WebsiteName"]}!")
                .AppendLine($"Before starting to use the website, you will have to follow some steps in order to confirm your email and configure your password.")
                .AppendLine($"Please click to the link bellow:")
                .AppendLine($"[{hostUrl}]({hostUrl})")
                .Send();

      return this.CreatedAtAction("Get", user.Id);
    }

    // PUT api/clients/5
    [HttpPut("{id}")]
    [Authorize("Users FullAccess")]
    public async Task<ActionResult> Put(string id, [FromBody]UserDetail value)
    {
      if (!ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      var user = await context.Users
          .Include(x => x.Claims)
          .SingleOrDefaultAsync(x => x.Id == id);

      if (user == null)
      {
        return this.NotFound();
      }

      this.UpdateClaims(user, value);

      await this.userManager.UpdateRolesAsync(user, value.Roles);
      await this.userManager.SetUserNameAsync(user, value.Email);
      await this.userManager.SetEmailAsync(user, value.Email);

      context.Update(user);

      await context.SaveChangesAsync();

      return this.Ok();
    }

    // DELETE api/clients/5
    [HttpDelete("{id}")]
    [Authorize("Users FullAccess")]
    public IActionResult Delete(string id)
    {
      var user = context.Users.Find(id);

      if(user == null){
        return BadRequest();
      }

      context.Users.Remove(user);
      context.SaveChanges();

      return Ok();
    }

    private void UpdateClaims(ApplicationUser user, UserDetail value)
    {
      user.Claims.UpdateFromFiltered(value.Claims.Where(x => x.ClaimType != ClaimTypes.Role), (IdentityUserClaim<string> e) => e.ClaimType != ClaimTypes.Role, (e, i) =>
      {
        return e.ClaimType == i.ClaimType;
      }
      , (e, i) =>
      {
        e.ClaimValue = i.ClaimValue;
        e.ClaimType = i.ClaimType;
      });
    }
  }
}
