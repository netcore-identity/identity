
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace Identity.Controllers
{
  using IdentityServer4.EntityFramework.DbContexts;
  using IdentityServer4.EntityFramework.Entities;
  using Microsoft.AspNetCore.Authorization;
  using Microsoft.AspNetCore.Mvc;
  using System.Collections.Generic;
  using System.Linq;
  using Identity.Data;
  using Identity.Models;
  using Microsoft.EntityFrameworkCore;
  using System;
  using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

  [Route("api/[controller]")]
  public class RolesController : Controller
  {
    ApplicationDbContext context;

    public RolesController(ApplicationDbContext context)
    {
      this.context = context;
    }

    // GET: api/clients
    [HttpGet]
    [Authorize("Roles Readonly")]
    public IEnumerable<RoleListItem> Get()
    {
      var result = context
          .Roles
          .ToArray()
          .Select(x => new RoleListItem {
              RoleName = x.Name
          });

      return result;
    }
  }
}
