using System;
using System.Collections.Generic;
using System.Linq;

namespace Identity.Mailing
{
    public class SmtpMailConfiguration{
        public string Server { get; set; }

        public int Port { get; set; }

        public bool UseSsl { get; set;}

        public string Login { get; set; }

        public string Password { get; set; }
    }

    public class MailConfiguration
    {
        public string From { get; set; }

        public string FromName { get; set; }

        public string SenderUrl { get; set; }

        public string Signature { get; set; }

        public SmtpMailConfiguration Smtp { get; set; }
    }
}