using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Options;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;
using System.Threading.Tasks;

namespace Identity.Mailing
{
  public class MailBuilder
  {
    private IOptions<MailConfiguration> configuration;

    private string subject;

    private IList<string> to;

    private IList<string> cc;

    private IList<string> bcc;

    private IList<string> body;

    public MailBuilder(IOptions<MailConfiguration> configuration)
    {
      this.configuration = configuration;
      this.to = new List<string>();
      this.cc = new List<string>();
      this.bcc = new List<string>();
      this.body = new List<string>();
    }

    public MailBuilder StartNew()
    {
      this.Reset();
      return this;
    }

    public MailBuilder To(string to)
    {
      if (String.IsNullOrWhiteSpace(to))
      {
        throw new ArgumentNullException(nameof(to));
      }

      this.to.Add(to);

      return this;
    }

    public MailBuilder Cc(string cc)
    {
      if (String.IsNullOrWhiteSpace(cc))
      {
        throw new ArgumentNullException(nameof(cc));
      }

      this.cc.Add(cc);

      return this;
    }

    public MailBuilder Bcc(string bcc)
    {
      if (String.IsNullOrWhiteSpace(bcc))
      {
        throw new ArgumentNullException(nameof(bcc));
      }

      this.bcc.Add(bcc);

      return this;
    }

    public MailBuilder Subject(string subject)
    {
      if (String.IsNullOrWhiteSpace(subject))
      {
        throw new ArgumentNullException(nameof(subject));
      }

      this.subject = subject;

      return this;
    }

    public MailBuilder AppendLine(string text)
    {
      if (text == null)
      {
        return this;
      }

      text.Split(new[] { Environment.NewLine }, StringSplitOptions.None)
        .ToList()
        .ForEach(this.body.Add);

      this.body.Add("<br />    ");

      return this;
    }

    public async Task Send()
    {
      if (!this.to.Any())
      {
        throw new ArgumentNullException(nameof(this.to));
      }

      var message = new MimeMessage();

      if (String.IsNullOrWhiteSpace(this.configuration.Value.FromName))
      {

        message.From.Add(new MailboxAddress(this.configuration.Value.From));
      }
      else
      {
        message.From.Add(new MailboxAddress(this.configuration.Value.FromName, this.configuration.Value.From));
      }

      message.From.Add(new MailboxAddress(this.configuration.Value.From));
      message.To.AddRange(this.to.Select(x => new MailboxAddress(x)));

      message.Subject = this.subject;

      message.Body = new TextPart("html")
      {
        Text = this.FormatBody()
      };

      using (var client = new SmtpClient())
      {
        await client.ConnectAsync(
          this.configuration.Value.Smtp.Server,
          this.configuration.Value.Smtp.Port,
          this.configuration.Value.Smtp.UseSsl);

        // Note: only needed if the SMTP server requires authentication
        client.Authenticate(this.configuration.Value.Smtp.Login, this.configuration.Value.Smtp.Password);

        await client.SendAsync(message);
        await client.DisconnectAsync(true);
      }
    }

    private string FormatBody()
    {
      var contents = File.ReadAllText(Path.Combine("Data", "Mails", "template.html"));

      StringBuilder builder = new StringBuilder();

      this.body.ToList()
        .ForEach(x =>
        {
          builder.AppendLine(x);
        });

      builder.AppendLine(this.configuration.Value.Signature);

      var result = contents
                      .Replace("<%SENDER_URL%>", this.configuration.Value.SenderUrl)
                      .Replace("<%MAIL_CONTENT%>", CommonMark.CommonMarkConverter.Convert(builder.ToString()));

      return result;
    }

    private void Reset()
    {
      this.to.Clear();
      this.cc.Clear();
      this.bcc.Clear();
      this.body.Clear();
    }

  }
}