

using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace Identity.Extensions
{
  public static class IUserManagerExtension
  {
    public static async Task UpdateRolesAsync<TUser>(this UserManager<TUser> userManager, TUser user, IEnumerable<string> roles)
     where TUser : class
    {
      if (user == null)
      {
        throw new System.ArgumentNullException(nameof(user));
      }

      if (roles == null)
      {
        throw new System.ArgumentNullException(nameof(roles));
      }

      var existingRoles = await userManager.GetRolesAsync(user);

      await userManager.RemoveFromRolesAsync(user, existingRoles.Where(x => !roles.Any(r => r == x)));
      await userManager.AddToRolesAsync(user, roles.Where(x => !existingRoles.Any(r => r == x)));
    }
  }
}