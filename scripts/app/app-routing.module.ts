import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/_core/login/login.component';
import { CoreRoutingModule } from './modules/_core/core-routing.module';
import { UnAuthorizedComponent } from './modules/_core/unauthorized/unauthorized.component';
import { HomeComponent } from './modules/home/home.component';
import { AuthGuard } from './modules/_core/guards/auth.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
