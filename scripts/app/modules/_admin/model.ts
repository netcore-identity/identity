export interface IUser {
  accessFailedCount?: number;
  claims?: Array<IIdentityClaim>;
  concurrencyStamp?: string;
  email?: string;
  emailConfirmed?: boolean;
  id?: string;
  lockoutEnabled?: boolean;
  lockoutEnd?: Date;
  logins?: Array<object>;
  normalizedEmail?: string;
  normalizedUserName?: string;
  passwordHash?: string;
  phoneNumber?: string;
  phoneNumberConfirmed?: boolean;
  roles?: string[];
  securityStamp?: string;
  twoFactorEnabled?: boolean;
  userName?: string;
}

export interface IIdentityRole {
  roleName: string;
}

export interface IIdentityClaim {
  id?: number;
  claimType: string;
  claimValue: string;
}
