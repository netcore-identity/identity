import { CommonModule  } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../_core/material.module';
import { CoreModule } from '../_core/core.module';

import { AdminNavBarComponent } from './admin-navbar.component';
import { AdminRoutingModule } from './admin-routing.module';

import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserRolesComponent, SelectRoleDialog } from './user-detail/roles/user-roles.component';
import { ClaimInputComponent } from './user-detail/claim-inputs/claim-input.component';
import { GenderClaimInputComponent } from './user-detail/claim-inputs/gender/gender-claim-input.component';
import { ClaimPipe } from './pipes/claim.pipe';

@NgModule({
    declarations: [
        ClaimPipe,
        AdminNavBarComponent,
        UsersComponent,
        UserRolesComponent,
        SelectRoleDialog,
        UserDetailComponent,
        ClaimInputComponent,
        GenderClaimInputComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AdminRoutingModule,
        CoreModule
    ],
    exports: [AdminNavBarComponent],
    entryComponents: [SelectRoleDialog]
})
export class AdminModule { }
