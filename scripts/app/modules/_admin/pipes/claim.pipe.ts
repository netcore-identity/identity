import { Pipe, PipeTransform } from '@angular/core';
import { IUser, IIdentityClaim } from '../model';
import { FormGroup } from '@angular/forms';

@Pipe({ name: 'claim' })
export class ClaimPipe implements PipeTransform {
  transform(value: (IUser | IIdentityClaim | FormGroup), claimType: string): string {
    if (value instanceof FormGroup) {
      return value.get('claims').value.find(x => x.claimType === claimType).claimValue;
    }

    if (value instanceof Array) {
      return value.find(x => x.claimType === claimType).claimValue;
    }

    return (<IUser>value).claims.find(x => x.claimType === claimType).claimValue;
  }
}
