import { Validators } from '@angular/forms';

// tslint:disable-next-line:max-line-length
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const UserDetailForm =  {
    email: [
        '',
        Validators.compose([
            Validators.required,
            Validators.pattern(EMAIL_REGEX),
        ])
    ],
    roles: [],
    claims: [],
};
