import { Component, OnDestroy, OnInit, Input, OnChanges, SimpleChange, Inject } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SecurityService } from '../../../_core/services/security.service';
import { IIdentityClaim, IUser, IIdentityRole } from '../../model';
import { FormControl, FormGroup } from '@angular/forms';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'user-roles',
  styleUrls: ['./user-roles.component.scss'],
  templateUrl: './user-roles.component.html'
})
export class UserRolesComponent implements OnInit, OnChanges {

  private _requestOptions: RequestOptionsArgs;

  @Input()
  user: FormGroup;

  newRole: string;

  displayedColumns = ['name', 'link'];
  roles: RoleClaimDataSource;

  constructor(public http: Http,
    private securityService: SecurityService,
    private dialog: MatDialog) {
    this.roles = new RoleClaimDataSource([]);

    const headers = new Headers();
    headers.append('Authorization', this.securityService.authorizationHeader());

    this._requestOptions = {
      headers: headers
    };
  }

  ngOnInit() {

  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if (!changes['user'] || !this.user) {
      this.roles = null;
      return;
    }

    this.roles = new RoleClaimDataSource(this.user.get('roles').value);
  }

  removeRole(role: string) {
    this.roles.remove(role);
  }

  selectRole() {
    this.http.get('/api/roles', this._requestOptions)
      .map(t => <IIdentityRole[]>t.json())
      .subscribe(t => {
        const dialogRef = this.dialog.open(SelectRoleDialog, {
          data: {
            claims: this.user.get('claims').value,
            roles: t.filter(r => this.user.get('roles').value.indexOf(r.roleName) < 0)
          }
        });

        dialogRef.afterClosed()
          .subscribe(result => {
            if (!result) {
              return;
            }
            this.roles.add(result);
          });
      });
  }
}

@Component({
  selector: 'select-role-dialog',
  templateUrl: './select-role-dialog.html',
})
export class SelectRoleDialog {
  rolesAutoComplete: FormControl = new FormControl();

  constructor(
    public dialogRef: MatDialogRef<SelectRoleDialog>,
    @Inject(MAT_DIALOG_DATA)
    public data: any) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close(this.rolesAutoComplete.value);
  }
}

export class RoleClaimDataSource extends DataSource<any> {
  filterChanged = new BehaviorSubject('');
  contentChanged = new BehaviorSubject<string[]>([]);

  get data() { return this.contentChanged.value; }

  get filter(): string { return this.filterChanged.value; }
  set filter(filter: string) { this.filterChanged.next(filter); }

  constructor(private roles: string[]) {
    super();

    this.setRoles();
  }

  connect(): Observable<string[]> {
    const observables = [
      this.contentChanged,
      this.filterChanged];

    return Observable.merge(...observables)
      .map(() => {
        return this.contentChanged.value;
      });
  }

  remove(role: string) {
    this.roles.splice(this.roles.indexOf(role), 1);
    this.setRoles();
  }

  add(value: string) {
    this.roles.push(value);
    this.setRoles();
  }

  disconnect() { }

  private setRoles() {
    this.contentChanged.next(this.roles);
  }
}
