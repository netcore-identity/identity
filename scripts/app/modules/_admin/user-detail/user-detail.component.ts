import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { SecurityService } from '../../_core/services/security.service';
import { IUser } from '../model';
import { UserDetailForm } from './user-detail-form';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    templateUrl: 'user-detail.component.html',
    styleUrls: ['user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

    private _requestOptions: RequestOptionsArgs;

    routeParams: ParamMap;
    load: Observable<FormGroup>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public http: Http,
        private formBuilder: FormBuilder,
        public securityService: SecurityService,
        public snackBar: MatSnackBar) {

        const headers = new Headers();
        headers.append('Authorization', this.securityService.authorizationHeader());

        this._requestOptions = {
            headers: headers
        };

        this.route.paramMap.subscribe(t => this.routeParams = t);
    }

    ngOnInit() {
        this.load = this.route.paramMap
            .mergeMap((r) => {
                if (!r.has('id')) {
                    return Observable.of({
                        claims: [],
                        email: '',
                        roles: []
                    });
                }

                return this.http.get(`/api/users/${r.get('id')}`, this._requestOptions)
                    .map(t => t.json());
            })
            .map(t => {
                const form = this.formBuilder.group(UserDetailForm);
                form.setValue(t);
                return form;
            });
    }

    send(form: FormGroup) {

        if (form.invalid) {
            return;
        }

        const user: IUser = form.getRawValue();

        this.route.paramMap
            .mergeMap(r => {
                if (!r.has('id')) {
                    return this.http.post(`/api/users`, user, this._requestOptions);
                }

                return this.http.put(`/api/users/${r.get('id')}`, user, this._requestOptions);
            })
            .map(t => {
                if (t.status === 201) {
                    this.router.navigate(['/admin/users/', t.text()])
                        .then(() => {
                            this.snackBar.open('User successfully created.', null, {
                                duration: 2000
                            });
                        });
                    return;
                }

                this.snackBar.open('User successfully updated.', null, {
                    duration: 2000
                });
            })
            .subscribe();
    }

    delete() {
        this.route.paramMap
            .mergeMap(r => {
                if (!r.has('id')) {
                    return;
                }

                return this.http.delete(`/api/users/${r.get('id')}`, this._requestOptions);
            })
            .map(t => {
                this.router.navigate(['/admin/users']);
            })
            .subscribe(t => {
                this.snackBar.open('User successfully deleted.', null, {
                    duration: 2000
                });
            });
    }
}

