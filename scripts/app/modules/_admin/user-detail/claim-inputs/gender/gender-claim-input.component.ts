import { Component, OnDestroy, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { IIdentityClaim, IUser } from '../../../model';
import { FormGroup } from '@angular/forms';

interface IGender {
    value: string|number;
    name: string;
}

@Component({
  selector: 'gender-claim-input',
  templateUrl: './gender-claim-input.component.html',
  styleUrls: ['./gender-claim-input.component.scss']
})
export class GenderClaimInputComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  claimType: string;

  @Input()
  label: string;

  @Input()
  model: IUser | FormGroup;

  claim?: IIdentityClaim;

  genders: Array<IGender> = [{
    name: 'Unknown',
    value: '0'
  }, {
    name: 'Male',
    value: '1'
  }, {
    name: 'Female',
    value: '2'
  }];

  userGender: IGender;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if (!changes['model'] || !this.model) {
      this.claim = {
        claimValue: '0',
        claimType: this.claimType,
        id: 0
      };
      return;
    }

    let claims: IIdentityClaim[] = null;

    if (this.model instanceof FormGroup) {
      claims = this.model.get('claims').value;
    } else {
      claims = this.model.claims;
    }

    this.claim = claims.find(x => x.claimType === this.claimType);

    if (this.claim) {
      return;
    }

    this.claim = {
      claimValue: '0',
      claimType: this.claimType,
      id: 0
    };

    claims.push(this.claim);
  }


  ngOnDestroy(): void {
  }
}
