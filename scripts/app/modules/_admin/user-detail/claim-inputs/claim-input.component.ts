import { Component, OnDestroy, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { IIdentityClaim, IUser } from '../../model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'claim-input',
  templateUrl: './claim-input.component.html',
  styleUrls: ['./claim-input.component.scss']
})
export class ClaimInputComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  claimType: string;

  @Input()
  label: string;

  @Input()
  model: IUser | FormGroup;

  claim?: IIdentityClaim;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if (!changes['model'] || !this.model) {
      this.claim = {
        claimValue: '',
        claimType: this.claimType,
        id: 0
      };
      return;
    }

    let claims: IIdentityClaim[] = null;

    if (this.model instanceof FormGroup) {
      claims = this.model.get('claims').value;
    } else {
      claims = this.model.claims;
    }

    this.claim = claims.find(x => x.claimType === this.claimType);

    if (this.claim) {
      return;
    }

    this.claim = {
      claimValue: '',
      claimType: this.claimType,
      id: 0
    };

    claims.push(this.claim);
  }


  ngOnDestroy(): void {
  }
}
