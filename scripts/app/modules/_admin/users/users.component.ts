import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SecurityService } from '../../_core/services/security.service';
import { IUser } from '../model';
import { DiacriticsService } from '../../_core/services/diacritics.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'users.component.html',
    styleUrls: ['users.component.scss']
})

export class UsersComponent implements OnInit {
    displayedColumns = ['link', 'email', 'givenname', 'surname'];
    users: UserDataSource;

    private _requestOptions: RequestOptionsArgs;
    @ViewChild(MatPaginator) paginator: MatPaginator
    @ViewChild('filter') filter: ElementRef;

    constructor(
        private http: Http,
        private router: Router,
        private securityService: SecurityService,
        private snackBar: MatSnackBar,
        private diacriticsService: DiacriticsService) {
        let headers = new Headers();
        headers.append('Authorization', this.securityService.authorizationHeader());

        this._requestOptions = {
            headers: headers
        };

        this.users = new UserDataSource(this.diacriticsService, []);
    }

    ngOnInit() {
        this.http.get('/api/users', this._requestOptions)
            .map(t => t.json())
            .subscribe(t => {
                this.users = new UserDataSource(this.diacriticsService, t);
            });

        Observable.fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(() => {
                if (!this.users) { return; }
                this.users.filter = this.filter.nativeElement.value;
            });
    }

    createUser() {
        this.router.navigate(['/admin/users', 'new']);
    }
}

export class UserDataSource extends DataSource<any> {
    _filterChange = new BehaviorSubject('');
    get filter(): string { return this._filterChange.value; }
    set filter(filter: string) { this._filterChange.next(filter); }

    constructor(private diacriticsService: DiacriticsService, private data: IUser[]) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<IUser[]> {
        const displayDataChanges = [
            this._filterChange,
          ];

        return Observable.merge(...displayDataChanges).map(() => {
            return this.data.slice()
            .filter(c => {
                var givenname = c.claims.find(x => x.claimType === 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname').claimValue;
                var surname = c.claims.find(x => x.claimType === 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname').claimValue;

                let searchStr = this.diacriticsService.removeDiacritics((c.userName + c.email + givenname + surname).toLowerCase());

                return searchStr.indexOf(this.diacriticsService.removeDiacritics(this.filter.toLowerCase())) != -1;
            })
        });
    }

    disconnect() { }
}