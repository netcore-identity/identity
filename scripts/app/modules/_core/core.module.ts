import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { OAuthModule } from 'angular-oauth2-oidc';

import { MaterialModule } from './material.module';

import { LoginComponent } from './login/login.component';
import { UnAuthorizedComponent } from './unauthorized/unauthorized.component';
import { AuthGuard } from './guards/auth.guard';

import { SecurityService } from './services/security.service';
import { IsAuthenticatedDirective } from './directives/is-authenticated.directive';
import { HasRoleDirective } from './directives/hasrole.directive';
import { CoreRoutingModule } from './core-routing.module';
import { LoadingSpinnerComponent } from './loading-spinner.component';
import { DiacriticsService } from './services/diacritics.service';

@NgModule({
  declarations: [
    IsAuthenticatedDirective,
    HasRoleDirective,
    LoadingSpinnerComponent,
    LoginComponent,
    UnAuthorizedComponent,
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterialModule,
    CoreRoutingModule
  ],
  exports: [HasRoleDirective, MaterialModule, LoadingSpinnerComponent],
  providers: [SecurityService, DiacriticsService, AuthGuard]
})
export class CoreModule { }
