import { OAuthService } from 'angular-oauth2-oidc';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http } from '@angular/http';

@Injectable()
export class SecurityService {

    private authenticated = new Subject<boolean>();

    constructor(private http: Http,
        private oauthService: OAuthService) {
        this.oauthService.issuer = `${window.location.origin}`;

        // The SPA's id. Register SPA with this id at the auth-server
        this.oauthService.clientId = 'identity';

        this.oauthService.oidc = false;

        // set the scope for the permissions the client should request
        // The auth-server used here only returns a refresh token (see below), when the scope offline_access is requested
        this.oauthService.scope = 'openid profile email offline_access roles identity';

        // Use setStorage to use sessionStorage or another implementation of the TS-type Storage
        // instead of localStorage
        this.oauthService.setStorage(localStorage);

        // Set a dummy secret
        // Please note that the auth-server used here demand the client to transmit a client secret, although
        // the standard explicitly cites that the password flow can also be used without it. Using a client secret
        // does not make sense for a SPA that runs in the browser. That's why the property is called dummyClientSecret
        // Using such a dummy secreat is as safe as using no secret.
        this.oauthService.dummyClientSecret = 'secret';

        // Load Discovery Document and then try to login the user
        const url = `${window.location.origin}/.well-known/openid-configuration`;

        this.oauthService.silentRefreshRedirectUri = `${window.location.origin}/silent-refresh.html`;

        this.oauthService.loadDiscoveryDocument(url)
            .then(() => {
                this.oauthService
                    .events
                    .filter(e => e.type === 'token_expires')
                    .subscribe(() => {
                        this.oauthService
                            .refreshToken();
                    });
                this.authenticated.next(this.isAuthenticated());
            });
    }

    public logOut() {
        this.oauthService.logOut(true);

        this.authenticated.next(false);
    }

    public authenticate(username: string, password: string) {
        return this.oauthService.fetchTokenUsingPasswordFlowAndLoadUserProfile(username, password)
            .then(x => {
                this.authenticated.next(x != null);
                return x;
            });
    }

    public onAuthenticated() {
        return this.authenticated.asObservable();
    }

    public isAuthenticated() {
        return this.oauthService.hasValidAccessToken();
    }

    public hasRole(roles: string[]): boolean {
        const identity = (<any>this.oauthService.getIdentityClaims());

        if (!identity) {
            return false;
        }

        const userRoles: Array<string> = identity.role;

        if (!roles || !roles.length) {
            return true;
        }

        if (!userRoles || !userRoles.length) {
            return false;
        }

        if (userRoles instanceof Array) {
            return userRoles.filter(x => roles.filter(r => r === x).length).length > 0;
        }

        return roles.filter(x => x === userRoles).length > 0;
    }

    public hasPermission(permission: string): boolean {
        const identity = (<any>this.oauthService.getIdentityClaims());

        if (!identity) {
            return false;
        }

        const userPermissions: Array<string> = identity['http://example.org/claims/permission'];

        if (!permission) {
            return true;
        }

        if (!userPermissions || !userPermissions.length) {
            return false;
        }

        if (userPermissions instanceof Array) {
            return userPermissions.filter(x => x === permission).length > 0;
        }

        return permission === userPermissions;
    }

    public authorizationHeader() {
        return this.oauthService.authorizationHeader();
    }
}
