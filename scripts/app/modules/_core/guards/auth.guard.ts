import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SecurityService } from '../services/security.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
        private service: SecurityService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (!this.service.isAuthenticated()) {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }

        if (!this.service.hasRole(route.data.roles)) {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: state.url } });
            return false;
        }

        return true;
    }
}
