import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { SecurityService } from '../services/security.service';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: SecurityService,
        public snackBar: MatSnackBar) { }

    ngOnInit() {
        this.service.logOut();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this.loading = true;

        this.service.authenticate(this.model.username, this.model.password)
        .then((token: any) => {
            this.router.navigate([this.returnUrl]);
        })
        .then(() => {
            this.loading = false;
        }, e => {
            let message = e;

            if (e.status === 400) {
                message = 'User name or password invalid! Please retry.';
            }

            this.snackBar.open(message, null, {
                duration: 2000
            })
                .afterOpened()
                .toPromise()
                .then(() => { this.loading = false; });
        });
    }
}
