import { Directive, OnInit, ElementRef, Input } from '@angular/core';

@Directive({
    selector: '[appIsAuthenticated]'
})
export class IsAuthenticatedDirective implements OnInit {

    constructor(private _elem: ElementRef) { }

    ngOnInit() {
        this.applyPermission();
    }

    applyPermission() {
        if (localStorage.getItem('access_token')) {
            return;
        }

        this._elem.nativeElement.remove();
    }
}
