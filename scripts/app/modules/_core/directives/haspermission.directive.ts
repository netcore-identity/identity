import { Directive, OnInit, OnDestroy, ElementRef, Input, HostBinding } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({
    selector: '[appHasPermission]'
})
export class HasPermissionDirective implements OnInit, OnDestroy  {

    @Input()
    appHasPermission: string;

    @HostBinding('style.display')
    display = 'inherit';

    private authenticationSubscription: Subscription;

    constructor(private _elem: ElementRef,
        private service: SecurityService) { }

    ngOnInit() {
        this.applyPermission();

        this.authenticationSubscription = this.service.onAuthenticated()
            .subscribe(() => this.applyPermission());
    }

    ngOnDestroy(): void {
        this.authenticationSubscription.unsubscribe();
    }

    applyPermission() {
        if (this.service.hasPermission(this.appHasPermission)) {
            this.display = 'inherit';
            return;
        }

        this.display = 'none';
    }
}
