import { Directive, OnInit, OnDestroy, ElementRef, Input, HostBinding } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({
    selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy  {

    @Input()
    appHasRole: string;

    @HostBinding('style.display')
    display = 'inherit';

    private authenticationSubscription: Subscription;

    constructor(private _elem: ElementRef,
        private service: SecurityService) { }

    ngOnInit() {
        this.applyPermission();

        this.authenticationSubscription = this.service.onAuthenticated()
            .subscribe(() => this.applyPermission());
    }

    ngOnDestroy(): void {
        this.authenticationSubscription.unsubscribe();
    }

    applyPermission() {
        if (this.service.hasRole(JSON.parse(this.appHasRole))) {
            this.display = 'inherit';
            return;
        }

        this.display = 'none';
    }
}
