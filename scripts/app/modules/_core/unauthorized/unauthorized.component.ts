import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { OAuthService } from 'angular-oauth2-oidc';

@Component({
    templateUrl: 'unauthorized.component.html',
    styleUrls: ['unauthorized.component.less']
})

export class UnAuthorizedComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private oauthService: OAuthService,
        public snackBar: MatSnackBar) { }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this.loading = true;
        this.oauthService.fetchTokenUsingPasswordFlowAndLoadUserProfile(this.model.username, this.model.password)
            .then((token: any) => {
                this.router.navigate([this.returnUrl]);
            })
            .then(() => {
                this.loading = false;
            }, e => {
                let message = e;

                if (e.status === 400) {
                    message = 'User name or password invalid! Please retry.';
                }

                this.snackBar.open(message, null, {
                    duration: 2000
                })
                    .afterOpened()
                    .toPromise()
                    .then(() => { this.loading = false; });
            });
    }
}
