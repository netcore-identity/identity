import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { OAuthModule } from 'angular-oauth2-oidc';

import { CoreModule } from '../_core/core.module';
import { AdminModule   } from '../_admin/admin.module';
import { HomeComponent } from './home.component';

@NgModule({
    declarations: [
        HomeComponent,
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        CoreModule,
        AdminModule
    ],
    exports: [HomeComponent]
})
export class HomeModule { }
