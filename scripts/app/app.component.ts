import { Component, OnDestroy, OnInit } from '@angular/core';
import { SecurityService } from './modules/_core/services/security.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  isAuthenticated = false;

  private authenticationSubscription: Subscription;

  constructor(private service: SecurityService) {
    this.isAuthenticated = this.service.isAuthenticated();

  }

  ngOnInit() {
    this.authenticationSubscription = this.service.onAuthenticated()
      .subscribe(v => this.isAuthenticated = v);
  }

  ngOnDestroy(): void {
    this.authenticationSubscription.unsubscribe();
  }
}
