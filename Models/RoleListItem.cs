
namespace Identity.Models
{
  using System.Collections.Generic;
  using System.ComponentModel.DataAnnotations;
  using System.Security.Claims;
  using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

  public class RoleListItem
  {
    [Required]
    public string RoleName { get; set; }
  }
}