

namespace Identity.Models
{
  public interface IEntity
  {
    int Id { get; set; }
  }
}