
namespace Identity.Models.Extensions
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Linq.Expressions;
  using System.Threading.Tasks;

  public static class ICollectionExtensions
  {
    public static void UpdateFrom<TEntity, TItem>(this ICollection<TEntity> source, IEnumerable<TItem> from, Action<TEntity, TItem> updater)
      where TEntity : IEntity, new()
      where TItem : class, IEntity
    {
      UpdateFrom(source, from, (e, i) => e.Id == i.Id, updater);
    }

    public static void UpdateFrom<TEntity, TItem>(this ICollection<TEntity> source, IEnumerable<TItem> from, Func<TEntity, TItem, bool> match, Action<TEntity, TItem> updater)
      where TEntity : new()
      where TItem : class
    {
      UpdateFromFiltered(source, from, x => true, match, updater);
    }

    public static void UpdateFromFiltered<TEntity, TItem>(this ICollection<TEntity> source, IEnumerable<TItem> from, Func<TEntity, bool> filter, Func<TEntity, TItem, bool> match, Action<TEntity, TItem> updater)
      where TEntity : new()
      where TItem : class
    {
      if (source == null)
      {
        throw new ArgumentNullException(nameof(source));
      }

      if (from == null)
      {
        throw new ArgumentNullException(nameof(from));
      }

      if (updater == null)
      {
        throw new ArgumentNullException(nameof(updater));
      }

      DeleteFrom(source, filter, from, match);
      UpdateFromInternal(source, filter, from, match, updater);
      CreateFrom(source, from, match, updater);
    }

    private static void UpdateFromInternal<TEntity, TItem>(ICollection<TEntity> source, Func<TEntity, bool> filter, IEnumerable<TItem> from, Func<TEntity, TItem, bool> match, Action<TEntity, TItem> updater)
      where TEntity : new()
      where TItem : class
    {
      source
          .Where(x => filter.Invoke(x))
          .Select(x => new
          {
            Target = x,
            Source = from.SingleOrDefault(c => match.Invoke(x, c))
          })
      .Where(x => x.Source != null)
      .ToList()
      .ForEach(x => updater(x.Target, x.Source));
    }

    private static void DeleteFrom<TEntity, TItem>(ICollection<TEntity> source, Func<TEntity, bool> filter, IEnumerable<TItem> from, Func<TEntity, TItem, bool> match)
      where TEntity : new()
      where TItem : class
    {
      source
        .Where(x => filter.Invoke(x))
        .Where(x => !from.Any(c => match(x, c)))
        .ToList()
        .ForEach(x => source.Remove(x));
    }

    private static void CreateFrom<TEntity, TItem>(ICollection<TEntity> source, IEnumerable<TItem> from, Func<TEntity, TItem, bool> match, Action<TEntity, TItem> updater)
      where TEntity : new()
      where TItem : class
    {
      from.Where(x => !source.Any(c => match(c, x)))
        .ToList()
        .ForEach(x =>
        {
          var entity = new TEntity();

          updater(entity, x);

          source.Add(entity);
        });
    }
  }
}