
namespace Identity.Models
{
  using System.Collections.Generic;
  using System.ComponentModel.DataAnnotations;
  using System.Security.Claims;
  using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

  public class UserDetail
  {
    [Required]
    [EmailAddress]
    public string Email { get; set; }

    [Required]
    public IEnumerable<UserDetailClaim> Claims { get; set; }

    [Required]
    public IEnumerable<string> Roles { get; set; }
  }

  public class UserDetailClaim: IEntity
  {
    public int Id { get; set; }

    [Required]
    public string ClaimType { get; set; }

    public string ClaimValue { get; set; }
  }
}